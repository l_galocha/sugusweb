---
title: "Acerca"
---

SUGUS es el grupo de usuarios y usuarias de Software Libre de la Escuela Superior de Ingeniería Informática de la Universidad de Sevilla.

### ¿Dónde estamos?

Escuela Técnica de Ingeniería Informática US, Sótano [AS.43]

### ¿Qué hacemos?

Realizamos numerosas actividades con el objetivo de *fomentar el uso y la participación en el Software Libre* dentro de la Universidad: realizamos talleres sobre la utilización de herramientas libres, impartimos charlas, colaboramos en eventos de la Universidad, etc.

Por otro lado también trabajamos por la *defensa de los derechos digitales* y la *libertad en la red*,  mediante actividades tanto en la universidad como en el Parlamento Europeo.

Puedes pasar por SUGUS cuando quieras, siempre estamos dispuestos a hablar o echarte una mano :)

### Algunos proyectos libres donde han trabajado compañeros

#### Propios

* [Minolobot](https://github.com/SUGUS-GNULinux/minolobot)
* [Flameshot](https://flameshot.js.org/) 
* [Solaria](https://gitlab.com/atorresm/solaria) 

#### Colaborando

* [Netfilter](https://www.netfilter.org/)
* [Tor](https://www.torproject.org/)
* [Godot Engine](https://godotengine.org/)

### Proyectos en los que colabora SUGUS

* [Concurso Universitario de Software Libre (CUSL)](https://www.concursosoftwarelibre.org)
