---
title: "Taller de Introducción a Vim"
date: 2018-12-03T17:23:06+02:00
tags: ["charlas"]
---

{{< image src="/images/2018-12-03.jpg" alt="Cartel del taller de Vim" position="center" style="border-radius: 8px; width: 100%" >}}

¡Aprende a escribir como una bala y en cualquier lado gracias a la potencia de Vim!

<!--more-->

Comienza a usar Vim desde cero: desde como moverte sin despegar los dedos del teclado hasta como hacer ediciones con solo una tecla. 

- Ponente: José María Guisado Gómez
- 11 de diciembre de 2018
- Aula A0.30
