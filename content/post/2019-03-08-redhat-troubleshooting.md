---
title: "RedHat Troubleshooting Competition"
date: 2019-03-08T19:34:03Z
tags: ["charlas"]
---

{{< image src="/images/2019-03-08.png" alt="Hello Friend" position="center" style="border-radius: 8px; width: 100%" >}}

¿Alguna vez te has enfrentado a alguna situación real con Linux en la que tú eras
la única persona que podía y debía resolverla? No te pierdas esta
"Troubleshooting Master Competition" 

<!--more-->

En este taller encontrarás problemas tales como sistemas Linux que no arrancan,
páginas web reales que no funcionan, corrupciones de sistemas, problemas de
configuración... ¡Participa, disfruta, aprende y gana premios!
