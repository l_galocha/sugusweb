# Integración Continua

Cualquier cambio(commits) sobre este repositorio ejecuta ciertos jobs según la
rama donde se reciban, toda la lógica de estos trabajos se define en el fichero
`.gitlab-ci.yml`.

Por ahora utilizamos los runners públicos que GitLab ofrece gratuitamente.
Puedes leer más sobre cómo funciona CI de GitLab en el post de
[quick start](https://docs.gitlab.com/ce/ci/quick_start/) e
[introduction](https://about.gitlab.com/2015/12/14/getting-started-with-gitlab-and-gitlab-ci/)

Principalmente existen dos stages: **build** y **deploy**. Que se definen al
principio.
```
stages:
  - build
  - deploy
```

## Build stage

Este stage se encarga de la generación de la página web usando una imagen de
Docker de Hugo

`test:` y `pages:` son los dos trabajos que forman parte del __build stage__

### test:

```
test:
  image: registry.gitlab.com/pages/hugo:latest
  stage: build
  script:
  - hugo
  except:
  - master
```

El job `test` sirve para ejecutar hugo sobre el repositorio, generando la
página web, pero sin generar [artefactos](https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html)

Sirve para saber si el cambio realizado da una correcta ejecución de Hugo y por
tanto, la página compila correctamente.

### pages:

```
pages:
  image: registry.gitlab.com/pages/hugo:latest
  stage: build
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - stage
  - master
```

Este job es igual que el anterior pero genera artefactos que podrán ser usados
en el stage de deploy. Para ello usamos la sección `artifacts` dentro del job.
```
artifacts:
  paths:
  - public
```

## Deploy stage

Este stage contiene un único job `deploy:` encargado de desplegar la página web
en los servidores de SUGUS.

Se usa `rsync` para la transferencia, que usa `ssh` por detrás. Se guardan las
credenciales y direcciones necesarias en **variables de entorno** de GitLab CI.

### deploy:

```
deploy:
  image: alpine:latest
  stage: deploy
  before_script:
  - apk update
  - 'which ssh-agent || ( apk update -y && apk add openssh-client rsync -y )'
  - eval $(ssh-agent -s)
  - ssh-add -D
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan "SSH_HOST_NAME" >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts
  script:
  - rsync -hrvz --delete --exclude=_ -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" public/ "$SSH_HOST_PATH"
  only:
  - master
```

Este job lee y añade al `ssh-agent` la clave privada para conectarse por ssh al
servidor donde se despliega la aplicación. Se usa `rsync` para la transferencia
de archivos que se leen del artefacto del job `pages:`
